package ejerciciospoo1;

public class EjerciciosPOO1 {

    public static void main(String[] args) {
        
        /* + Añadir a la clase Persona un String "puesto". Usarlo solo para valores "ordeño","carniceria" o "gerencia".
           + Añadir a la clase Persona un Float "sueldo", que expresará el sueldo en euros.
           + Añadir un método cuantoGana a la clase Persona, que devuelva el String "Gana"+sueldo+" €"
           + Añadir un método proporcionSueldo a la clase Persona, que reciba una persona por argumentos, y devuelva un float con cuántas veces gana esa persona el sueldo de this.
           - Añadir un método ajustaSueldoA, que reciba un objeto Persona, y asigne el sueldo de this, poniéndolo en proporción al del objeto que recibe por parámetro. Un/a ordeñador/a debería ganar un 25% más que alguien de carnicería, y un/a gerente un 50% más que alguien de ordeño.
           + Crear la clase Vaca, con Strings para nombre, número de serie y función ("ordeño" o "carniceria"), un Float peso, un int edad, y un array de float con litros ordeñados en el día.
           + Crear un método asignaFunción a una vaca, de forma que si pesa más de 200kg, se asigne a carnicería, y si pesa menos, a ordeño.
           + Añadir a la clase persona un Array de Vacas
           - Crear en la clase persona un método quedarmeVacas(Vaca[] vac), que copie en el array interno a las vacas que corresponden a su puesto de trabajo.
        */
        Persona p1 = new Persona();
        p1.nombre = "Álvaro";
        p1.apellidos = "De Francisco Sánchez";
        p1.edad = 25;
        p1.altura = 1.77f;
        p1.puesto = "Gerencia";
        p1.sueldo = 2500.00f;
        
        Persona p2 = new Persona();
        p2.nombre = "Sara";
        p2.apellidos = "Pedrosa Ruiz";
        p2.edad = 24;
        p2.altura = 1.65f;
        p2.puesto = "Gerencia";
        p2.sueldo = 2500.00f;
        
        Persona p3 = new Persona();
        p3.nombre = "Antonio";
        p3.puesto = "Carnicería";
        p3.sueldo = p3.ajustaSueldoA(p1);
        
        Persona p4 = new Persona();
        p4.nombre = "Lucas";
        p4.puesto = "Ordeñador";
        p4.sueldo = 1000.00f;
        
        System.out.println("La proporción de sueldo de " + p1.nombre + " respecto a " + p2.nombre + " es: " + p1.proporcionSueldo(p2));
        System.out.println(p3.ajustaSueldoA(p1));
    }
    
}
