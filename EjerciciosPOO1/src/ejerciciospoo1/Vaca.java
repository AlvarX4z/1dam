package ejerciciospoo1;

public class Vaca {
    String nombre;
    String numSerie;
    String funcion;
    float peso;
    int edad;
    float [] litrosOrdennadosDia;
    
    public String asignaFuncion() {
        if (this.peso > 200) {
            this.funcion = "Carnicería";
        } else {
            this.funcion = "Ordeño";
        }
        return this.funcion;
    }
}
